#! /bin/sh

LOGFILE=${PWD}/logs.txt
function logger() {
	now="$(date +'%d/%m/%Y')";
	echo "----- ${now} -----" >> $LOGFILE;
}


cd ~/dev-env-exam;
logger;
git pull >> $LOGFILE;
