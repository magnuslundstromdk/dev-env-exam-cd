const { exec } = require('child_process');
const express = require('express');

const app = express();

app.post('/', (req,res) => {
	if(req.headers['x-gitlab-token'] === 'testink') {
		exec('./script.sh', 
		(err, stdout, stderr) => {
			if(err) {
				res.sendStatus(404);
			} else {
				res.sendStatus(200);
			}
		});
	} else {
		res.sendStatus(401);
	}
});

app.listen(9000, () => {
	console.log('Listening on port 9000');
});
